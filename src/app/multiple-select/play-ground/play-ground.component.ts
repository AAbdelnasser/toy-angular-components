import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-play-ground',
  templateUrl: './play-ground.component.html',
  styleUrls: ['./play-ground.component.scss']
})
export class PlayGroundComponent implements OnInit {

  options = [
    {id: 1, name: 'meter-1', building: {id: 1, name: 'building-1'}, state: {id: 'tx', name: 'TX'}},
    {id: 2, name: 'meter-2', building: {id: 1, name: 'building-1'}, state: {id: 'tx', name: 'TX'}},
    {id: 3, name: 'meter-alone-1', building: {id: 2, name: 'building-2'}, state: {id: 'tx', name: 'TX'}},
    {id: 4, name: 'meter-alone-2', building: {id: 3, name: 'building-3'}, state: {id: 'ny', name: 'NY'}},
  ];

  groupBy = ['state', 'building'];

  metersFC = new FormControl([{id: 3}]);

  constructor() { }

  ngOnInit() {
  }

}
