import { Component, forwardRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

function unique(values: any[]): any[] {
  return values.filter((v, i) => values.indexOf(v) === i);
}

class Option {
  isSelected = false;

  constructor(private parent: Group) {
  }

  toggleSelection() {
    throw Error('"toggleSelection" method is not implemented');
  }

  select() {
    throw Error('"select" method is not implemented');
  }

  deselect() {
    throw Error('"deselect" method is not implemented');
  }

  notifyParent() {
    if (this.parent) {
      this.parent.onChildToggled();
    }
  }
}

class Child extends Option {

  constructor(public obj, parent: Group) {
    super(parent);
  }

  toggleSelection() {
    this.isSelected = !this.isSelected;
    this.notifyParent();
  }

  select() {
    this.isSelected = true;
  }

  deselect() {
    this.isSelected = false;
  }
}

class Group extends Option {
  isGroup = true;
  children: Child[] | Group[];

  get flatOptions(): Child[] {
    const children = this.children;
    const isGroupChildren = (children[0] as Group).isGroup;
    if (!isGroupChildren) {
      return children;
    }
    return (children as Group[]).reduce((childrenFlatArr, g) => {
      return [].concat(childrenFlatArr, g.flatOptions);
    }, []);
  }

  constructor(public obj, children: any[], parent: Group) {
    super(parent);
    this.children = children.map(c => new Child(c, this));
  }

  toggleSelection() {
    this.isSelected ? this.deselect() : this.select();
    this.notifyParent();
  }

  select(updateChildren = true) {
    this.isSelected = true;
    if (updateChildren) {
      this.children.forEach(c => c.select());
    }
  }

  deselect(updateChildren = true) {
    this.isSelected = false;
    if (updateChildren) {
      this.children.forEach(c => c.deselect());
    }
  }

  onChildToggled() {
    const childrenSelectionStatuses: boolean[] = (this.children as Option[]).map(c => c.isSelected);
    const anyOptionIsNotSelected = childrenSelectionStatuses.some(s => !s);
    if (anyOptionIsNotSelected && this.isSelected) {
      this.deselect(false);
      this.notifyParent();
      return;
    }

    const allOptionsSelected = childrenSelectionStatuses.every(s => !!s);
    if (allOptionsSelected && !this.isSelected) {
      this.select(false);
      this.notifyParent();
      return;
    }
  }
}

class MultipleSelectModel {
  mappedOptions: Group[];

  get flatOptions(): Child[] {
    return this.mappedOptions.reduce((optionsArr, g) => {
      return [].concat(optionsArr, g.flatOptions);
    }, []);
  }

  get selectedOptionsValues(): any[] {
    return this.flatOptions.filter(o => o.isSelected).map(o => o.obj);
  }

  constructor(private options: any[], private groupBy: string[], private hasAllOption = true) {
    this.generateOptions();
  }

  static generateGroups(options: { [key: string]: any }[], groupByArr: string[], parent = null) {
    return groupByArr.reduce((accOptions, groupByElem, i) => {
      if (i === 0) {
        return MultipleSelectModel.generateGroup(options, groupByElem, parent);
      }
      return (accOptions as Group[]).map(g => {
        const childrenOptions = (g.children as Child[]).map(c => c.obj);
        g.children = MultipleSelectModel.generateGroup(childrenOptions, groupByElem, g);
        return g;
      });
    }, []);
  }

  // say that unique by ['building'] that means option should have obj in 'building' and contains id, displayName
  static generateGroup(options: { [key: string]: any }[], groupBy: string, parent = null) {
    const groupsIds = options.map(o => o[groupBy].id);
    const uniqueGroupsIds = unique(groupsIds);
    return uniqueGroupsIds.map(gId => {
      const groupOptions = options.filter(o => o[groupBy].id === gId);
      const groupObj = groupOptions[0][groupBy];
      return new Group(groupObj, groupOptions, parent);
    });
  }

  private generateOptions() {
    if (!this.groupBy) {
      //  TODO Abdalla 06 Apr 2020 : to be handled later
    }
    this.mappedOptions = MultipleSelectModel.generateGroups(this.options, this.groupBy);
  }

  private resetAll() {
    this.mappedOptions.forEach(g => g.deselect());
  }

  select(selectedOptions: { id: any }[]) {
    this.resetAll();
    const allOptions = this.flatOptions;
    selectedOptions.forEach(optionVal => {
      const option = allOptions.find(o => o.obj.id === optionVal.id);
      option.toggleSelection();
    });
  }
}

@Component({
  selector: 'app-multiple-select-input',
  templateUrl: './multiple-select-input.component.html',
  styleUrls: ['./multiple-select-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultipleSelectInputComponent),
      multi: true,
    },
  ],
})
export class MultipleSelectInputComponent implements OnInit, OnChanges, ControlValueAccessor {

  multipleSelectModel: MultipleSelectModel;

  selectedOptions: any[];
  updateSelectedOptions: (selectedOption: any[]) => void;

  @Input() options: any[];
  @Input() groupBy: string[];
  @Input() hasAllOption = true;

  constructor() { }

  ngOnInit() {
    this.multipleSelectModel = new MultipleSelectModel(this.options, this.groupBy);
  }

  ngOnChanges({options}: SimpleChanges): void {
    if (options.previousValue !== options.currentValue && !options.firstChange) {
      this.multipleSelectModel = new MultipleSelectModel(this.options, this.groupBy, this.hasAllOption);
      if (this.selectedOptions) {
        this.multipleSelectModel.select(this.selectedOptions);
      }
    }
  }

  writeValue(obj: any): void {
    this.selectedOptions = obj;
    if (this.multipleSelectModel) {
      this.multipleSelectModel.select(this.selectedOptions);
    }
  }

  registerOnChange(fn: any): void {
    this.updateSelectedOptions = fn;
  }

  registerOnTouched(fn: any): void {
  }

  onOptionClick(option: Group | Child) {
    option.toggleSelection();
    this.selectedOptions = this.multipleSelectModel.selectedOptionsValues;
    this.updateSelectedOptions(this.selectedOptions);
  }

}
