import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultipleSelectInputComponent } from './multiple-select-input/multiple-select-input.component';
import { PlayGroundComponent } from './play-ground/play-ground.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [MultipleSelectInputComponent, PlayGroundComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
    ],
  exports: [PlayGroundComponent],
})
export class MultipleSelectModule { }
