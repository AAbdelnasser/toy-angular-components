import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent {
  stepsArr: Array<any>;

  @Input()
  set stepsCount(count: number) {
    this.stepsArr = new Array<any>(count);
  }

  @Input() activeStep: number;
}
