import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, from, interval, Observable, timer } from 'rxjs';
import { delay, map, switchMap, take } from 'rxjs/operators';
import { tap } from 'rxjs/internal/operators/tap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'toy-application';

  stepDefs: { i: number; title: string; }[] = [
    {i: 0, title: 'Select Orders'},
    {i: 1, title: 'Preparing Orders'},
    {i: 2, title: 'Out for Delivery'},
    {i: 3, title: 'Delivered'},
  ];

  startRotating$ = new BehaviorSubject(null);
  step$: Observable<{ i: number; title: string; }> = this.startRotating$.pipe(
    tap(() => console.log('update')),
    switchMap(() => {
      return interval(1000).pipe(
        take(this.stepDefs.length),
        map(i => this.stepDefs[i]),
      );
    }),
  );

  ngOnInit(): void {

  }

  startRouting() {
    this.startRotating$.next(null);
  }
}
